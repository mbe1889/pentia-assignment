import React from 'react'
import { ReactComponent as HeroIcon } from '../assets/zeppeliner.svg'

export const Hero = () => (
  <section className='section hero' data-theme='grey'>
    <div className='hero__container'>
      <HeroIcon className='hero__icon' />
      <h1 className='heading heading--large'>Digital transformation</h1>
      <p className='sub-heading'>Rådgivning. Implementering. Resultater</p>
      <p className='text'>Hvordan bliver din virksomhed en digital vinder i fremtiden?<br />Hvilke tiltag skal der til strategisk og taktisk for at dreje forretningen i den rigtige retning? Hvilke elementer indeholder en succesfuld digital transformation? Vi har svarene. Pentia kan hjælpe dig på hele rejsen fra strategisk rådgivning til implementering.</p>
    </div>
    <div className='scroll'>
      <p>Start din rejse her</p>
      <div className='scroll__dots'>
        <div className='scroll__dot' />
        <div className='scroll__dot' />
        <div className='scroll__dot' />
      </div>
    </div>
  </section>
)

export default Hero
