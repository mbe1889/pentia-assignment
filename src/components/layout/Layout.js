import React from 'react'
import Navigation from './Navigation'

export const Layout = ({ children }) => (
  <>
    <Navigation />
    <main>{children}</main>
  </>
)

export default Layout
