import React, { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { ReactComponent as Logo } from '../../assets/logo.svg'
import useNoScroll from '../../hooks/useNoScroll'

function Navigation () {
  const menuOpen = useSelector(state => state.ui.menuOpen)
  const dispatch = useDispatch()
  const toggleMenu = useCallback(() => dispatch({ type: 'TOGGLE_MENU', payload: menuOpen }), [dispatch, menuOpen])
  useNoScroll(menuOpen)

  return (
    <>
      <div className={`nav ${menuOpen ? 'nav--open' : ''}`}>
        <Logo className='logo' />
        <div className='menu-trigger' onClick={toggleMenu}>
          <span />
          <span />
          <span />
          <span />
        </div>
      </div>
      <div className={`menu ${menuOpen ? 'menu--open' : ''}`}>
        <ul className='menu__list'>
          <li className='menu__link'>Cases</li>
          <li className='menu__link'>Services</li>
          <li className='menu__link'>Kerriere</li>
          <li className='menu__link'>Nyt</li>
          <li className='menu__link'>Events</li>
          <li className='menu__link'>Om Pentia</li>
        </ul>
      </div>
    </>
  )
}

export default Navigation
