import React from 'react'
import { useForm, useField } from 'react-final-form-hooks'

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const onSubmit = async values => {
  await sleep(3000)
  console.log(JSON.stringify(values, 0, 2))
}

const Form = () => {
  const { form, handleSubmit, submitting } = useForm({
    onSubmit
  })
  const name = useField('name', form)
  const mobile = useField('mobile', form)
  const email = useField('email', form)
  const zipCode = useField('zip-code', form)
  const city = useField('city', form)

  return (
    <div className='form-wrap'>
      <div className={`form__message ${submitting ? 'form__message--visible' : ''}`}>
        <p>Tak for din interesse.<br />Vi kontakter dig!</p>
      </div>
      <form onSubmit={handleSubmit} className={`form ${submitting ? 'form--hidden' : ''}`}>
        <input {...name.input} className='input' type='text' aria-label='name' id='name' placeholder='Navn' required />
        <input {...mobile.input} className='input' type='tel' aria-label='mobile' id='mobile' pattern='^[0-9]*$' placeholder='Mobil' required />
        <input {...email.input} className='input' type='email' aria-label='email' id='email' placeholder='Email' required />
        <input {...zipCode.input} className='input' type='text' aria-label='zip-code' id='zip-code' pattern='^[0-9]*$' placeholder='Postnr.' required />
        <input {...city.input} className='input' type='text' aria-label='city' id='city' placeholder='By' required />
        <button type='submit' className='submit' disabled={submitting}>Bliv kontaktet</button>
      </form>
    </div>
  )
}

export default Form
