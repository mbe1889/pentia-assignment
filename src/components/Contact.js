import React from 'react'
import Form from './Form'
import { ReactComponent as AwardIcon } from '../assets/prize.svg'

function Contact () {
  return (

    <section className='section section--middle' data-theme='red'>
      <div className='container'>
        <h2 className='heading'>Få 10 gode råd om digital transformation</h2>
        <p className='sub-heading'>Ja tak, jeg vil gerne høre mere om digital transformation</p>
        <Form />
      </div>
      <div className='award'>
        <AwardIcon className='award__icon' />
        <p>Pentia vinder pris for digital innovation 2017</p>
      </div>
    </section>
  )
}

export default Contact
