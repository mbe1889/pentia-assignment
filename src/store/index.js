import { createStore, combineReducers } from 'redux'
import reducers from './reducers'

const enhancers = []
const isProduction = process.env.NODE_ENV === 'production' && process.env.BUILD_CONFIGURATION === 'Production'
if (isProduction === false) {
  // only enable redux devtools in development environments
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__
  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

const rootReducer = combineReducers(reducers)
const store = createStore(rootReducer, ...enhancers)

export default store
