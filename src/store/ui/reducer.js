import { produce } from 'immer'
import { TOGGLE_MENU } from '../actionTypes'

const initialState = Object.freeze({
  menuOpen: false
})

export default (state = initialState, action) => produce(state, ui => {
  switch (action.type) {
    case TOGGLE_MENU:
      ui.menuOpen = !action.payload
      break
    default: return ui
  }
  return ui
})
