import { connectRouter } from 'connected-react-router'
import history from '../history'
import ui from './ui/reducer'

const reducers = {
  router: connectRouter(history),
  ui
}

export default reducers
