import React from 'react'
import Hero from '../components/Hero'
import Contact from '../components/Contact'
import { ReactComponent as StrategyIcon } from '../assets/illustration1.svg'
import { ReactComponent as ExecutionIcon } from '../assets/illustration2.svg'

export const Home = () => (
  <>
    <Hero />
    <section className='section section--middle services'>
      <div className='container'>
        <h2 className='heading'>Vi kan hjælpe dig med digital transformation på to fronter</h2>
        <div className='row'>
          <div className='col'>
            <StrategyIcon className='services__icon' />
            <h3 className='heading heading--small'>Strategisk rådgivning om digital transformation</h3>
            <p className='text'>Udnytter din virksomhed sit digitale potentiale? Hvilke forretningsmuligheder er der, og hvad betyder det på et strategisk plan?</p>
          </div>
          <div className='col'>
            <ExecutionIcon className='services__icon' />
            <h3 className='heading heading--small'>Eksekvering af digital transformation</h3>
            <p className='text'>I ved, hvad I vil, men I har brug for hjælp til at rulle strategien ud i forhold til organisationen og det digitale fundament bl.a. på Sitecore. Det handler om mennesker og resultater.</p>
          </div>
        </div>
      </div>
    </section>

    <Contact />
  </>
)

export default Home
