import React, { Suspense } from 'react'
import { ConnectedRouter } from 'connected-react-router'
import { Route } from 'react-router-dom'
import { Switch } from 'react-router-dom'
import history from './history'
import Layout from './components/layout/Layout'

import Home from './containers/Home'

const App = () => {
  return (
    <ConnectedRouter history={history}>
      <Layout>
        <Suspense fallback={''}>
          <Switch>
            <Route path='/' component={Home} />
          </Switch>
        </Suspense>
      </Layout>
    </ConnectedRouter>
  )
}

export default App
