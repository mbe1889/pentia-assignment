This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Pentia - Assignment

Landingpage prototype build with React + Redux ( Overcomplicated FE Stack for this project )

## Available Scripts

Using yarn - In the project directory, you can run:

### `yarn`

Installing all dependencies

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Follow comandline instructions to serve the production build locally
Your app is ready to be deployed!
